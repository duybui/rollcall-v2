//
//  AppDelegate.swift
//  rollcall-v2
//
//  Created by Duy Bui on 7/28/18.
//  Copyright © 2018 DuyBui. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    return true
  }
}
